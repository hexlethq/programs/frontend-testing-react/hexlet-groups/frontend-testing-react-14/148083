// {
//   amount: '464.23',
//     date: 2012-02-02T00:00:00.000Z,
//   business: 'Mohr Group',
//   name: 'Money Market Account 3832',
//   type: 'deposit',
//   account: '12043000'
// }

const faker = require('faker');

// BEGIN
describe('test dumb', () => {
  test('Function is not responsive to incorrect data', () => {
    const data = faker.helpers.createTransaction(null);
    expect(data).toBeInstanceOf(Object);
  });

  test('Response object has all keys', () => {
    const data = Object.keys(faker.helpers.createTransaction());
    const resposeKeysArr = ['amount', 'date', 'business', 'name', 'type', 'account'];
    expect(data).toEqual(expect.arrayContaining(resposeKeysArr));
  });

  test('Amount has string type', () => {
    const data = faker.helpers.createTransaction();
    expect(typeof data.amount).toBe('string');
  });

  test('Amount is bigger than zero', () => {
    const data = faker.helpers.createTransaction();
    expect(Number(data.amount)).toBeGreaterThanOrEqual(0);
  });

  test('Data has expected type', () => {
    const data = faker.helpers.createTransaction();
    expect(data.date).toBeInstanceOf(Date);
  });

  test('Data has expected value', () => {
    const data = faker.helpers.createTransaction();
    const mockDate = new Date('2012-02-02T00:00:00.000Z');
    expect(data.date.toString()).toEqual(mockDate.toString());
  });

  test('Business field is not empty', () => {
    const data = faker.helpers.createTransaction().business;
    expect(data.length).toBeTruthy();
    expect(data.length).toBeGreaterThan(0);
  });

  test('Name field is not empty', () => {
    const data = faker.helpers.createTransaction().name;
    expect(data.length).toBeTruthy();
    expect(data.length).toBeGreaterThan(0);
  });

  test('Type field is not empty', () => {
    const data = faker.helpers.createTransaction().type;
    expect(data.length).toBeTruthy();
    expect(data.length).toBeGreaterThan(0);
  });

  test('Account has string type', () => {
    const data = faker.helpers.createTransaction();
    expect(typeof data.account).toBe('string');
  });

  test('Account is bigger than zero', () => {
    const data = faker.helpers.createTransaction();
    expect(Number(data.account)).toBeGreaterThanOrEqual(0);
  });
});

describe('test matcher', () => {
  test('test1', () => {
    const data = faker.helpers.createTransaction();
    expect(data).toMatchObject({
      amount: expect.any(String),
      date: expect.any(Date),
      name: expect.any(String),
      type: expect.any(String),
      account: expect.any(String),
    });
  });
});
// END
