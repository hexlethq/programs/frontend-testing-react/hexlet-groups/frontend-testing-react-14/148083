// BEGIN
const faker = require('faker');
require('expect-puppeteer');

const port = '5001';
const appUrl = `http://localhost:${port}}`;
const appArticlesUrl = `http://localhost:${port}/articles`;
const appNewArticleUrl = `http://localhost:${port}/articles/new`;

describe('E2E', () => {
  beforeAll(async () => {
    await page.goto(`${appUrl}`);
  });

  test('Page loaded', async () => {
    // https://pptr.dev/#?product=Puppeteer&version=v9.1.1&show=api-pageevaluatepagefunction-args
    await page.goto(appUrl);
    const title = await page.$('#title');
    const pageTitle = await page.evaluate((el) => el.innerText, title);
    expect(pageTitle).toContain('Welcome to a Simple blog!');
  });

  test('Records have been loaded and their number is more than 0', async () => {
    // https://pptr.dev/#?product=Puppeteer&version=v9.1.1&show=api-pageselector-1
    await page.goto(`${appArticlesUrl}`);
    const listItems = await page.$$('#articles tr');
    expect(listItems.length).toBeGreaterThan(0);
  });

  test('We can click on the create article button and see the form', async () => {
    // https://pptr.dev/#?product=Puppeteer&version=v9.1.1&show=api-pagewaitforselectorselector-options
    await page.goto(`${appArticlesUrl}`);
    await page.click('[data-test-id="new-article-btn"]');
    await expect(page.waitForSelector('[data-test-id="create-article-form"]')).resolves.toBeTruthy();
  });

  test('Can fill out a form and create a new article', async () => {
    // https://pptr.dev/#?product=Puppeteer&version=v9.1.1&show=api-pageevalselector-pagefunction-args-1
    await page.goto(`${appNewArticleUrl}`);

    const articleSettings = {
      name: faker.name.title(),
      category: '1',
      content: faker.lorem.paragraph(),
    };

    await (await page.$('#name')).type(articleSettings.name);
    await (await page.$('#category')).select(articleSettings.category);
    await (await page.$('#content')).type(articleSettings.content);
    await page.click('[data-test-id="submit-article-btn"]');
    await page.waitForSelector('[data-test-id="articles-table"]');
    const result = await page.$eval(
      'tbody > tr[data-test-id="articles-table-row"]:last-child > td[data-test-id="article-name"]',
      (el) => el.innerText,
    );
    expect(result).toContain(articleSettings.name);
  });

  test('We can edit the article. After that, the data on the page with all the articles changes.', async () => {
    const articleSettings = {
      name: faker.name.title(),
      category: '1',
      content: faker.lorem.paragraph(),
    };

    await page.goto(`${appArticlesUrl}`);
    await page.click('tbody > tr:nth-child(1) a[data-test-id="article-edit"]');
    await page.waitForSelector('#edit-form');
    // eslint-disable-next-line no-param-reassign
    await page.$eval('#name', (el) => { el.value = ''; });
    await page.type('#name', articleSettings.name);
    await page.click('input[type=submit]');
    await page.waitForSelector('#articles');
    const result = await page.$eval(
      'tbody > tr[data-test-id="articles-table-row"]:nth-child(1) > td[data-test-id="article-name"]',
      (el) => el.innerText,
    );

    expect(result).toBe(articleSettings.name);
  });
});

// END
