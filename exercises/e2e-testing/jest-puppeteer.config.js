module.exports = {
  server: {
    command: 'npm start',
    port: 5001,
    launchTimeout: 5000,
  },
  launch: {
    defaultViewport: {
      width: 1920,
      height: 1080,
    },
    args: [
      '--no-sandbox',
    ],
  },
};
