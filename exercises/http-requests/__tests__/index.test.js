const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');

const BASE_API_URI = 'https://example.com/users';
const BASK_API_URL = new URL(BASE_API_URI);

const MOCK_RESPONSE_DATA = {
  firstname: 'Fedor',
  lastname: 'Sumkin',
  age: 33,
};

// BEGIN

nock.disableNetConnect();

beforeEach(() => {
  nock.enableNetConnect(BASK_API_URL.origin);
});

afterEach(() => {
  nock.cleanAll();
});

describe('positive cases', () => {
  // CREATE

  test('POST READ OK', async () => {
    nock(BASK_API_URL.origin).post(BASK_API_URL.pathname).reply(201, MOCK_RESPONSE_DATA);
    const response = await post(BASK_API_URL.href, { ...MOCK_RESPONSE_DATA });
    expect(response.data).toEqual(MOCK_RESPONSE_DATA);
    expect(response.status).toBe(201);
  });

  test('GET READ OK', async () => {
    nock(BASK_API_URL.origin).get(BASK_API_URL.pathname).reply(200, MOCK_RESPONSE_DATA);
    const response = await get(BASK_API_URL.href);
    expect(response.data).toEqual(MOCK_RESPONSE_DATA);
    expect(response.status).toBe(200);
  });
});

describe('negative cases', () => {
  test('CONNECTION LOST', async () => {
    nock.disableNetConnect();
    await expect(get(BASK_API_URL.href)).rejects.toThrow();
    await expect(post(BASK_API_URL.href, MOCK_RESPONSE_DATA)).rejects.toThrow();
  });

  test('404', async () => {
    nock(BASK_API_URL.origin).get(`${BASK_API_URL.pathname}/user-list`).reply(404, {});
    await expect(get(`${BASK_API_URL.href}/admin`)).rejects.toThrow();
  });

  test('500', async () => {
    nock(BASK_API_URL.origin).get(`${BASK_API_URL.pathname}/admin`).reply(500, {});
    await expect(get(`${BASK_API_URL.href}/admin`)).rejects.toThrow();
  });
});

// END
