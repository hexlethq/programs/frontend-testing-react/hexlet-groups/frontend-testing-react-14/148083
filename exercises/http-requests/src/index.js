const axios = require('axios');

// BEGIN
const get = (url) => axios.get(url);

const post = (url, data) => axios.post(url, data);
// END

module.exports = { get, post };
