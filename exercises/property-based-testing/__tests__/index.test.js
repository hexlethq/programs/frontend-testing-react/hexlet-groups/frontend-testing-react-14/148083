const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

test('Compare increment sorting', () => { // eslint-disable-line
  fc.assert(
    fc.property(
      fc.int32Array({
        max: 100,
        maxLength: 10,
        min: 0,
        minLength: 10,
      }), (generatedArray) => {
        const sortedArr = sort(generatedArray);
        const reversedArr = generatedArray.slice().sort((f, s) => s - f);
        expect(sortedArr).toEqual(reversedArr.reverse());// eslint-disable-line
      },
    ),
  );
});

test('Check for array length', () => { // eslint-disable-line
  fc.assert(
    fc.property(
      fc.int32Array({
        max: 100,
        maxLength: 10,
        min: 0,
        minLength: 10,
      }), (generatedArray) => {
        const generatedArrLength = generatedArray.length;
        const sortedArrLength = sort(generatedArray).length;
        expect(generatedArrLength).toBe(sortedArrLength); // eslint-disable-line
      },
    ),
  );
});

test('Idempotency test', () => { // eslint-disable-line
  fc.assert(
    fc.property(
      fc.int32Array({
        max: 100,
        maxLength: 10,
        min: 0,
        minLength: 10,
      }), (generatedArray) => {
        const sortedArr = sort(generatedArray);
        const sortedArr2 = sort(generatedArray);
        expect(sortedArr).toEqual(sortedArr2); // eslint-disable-line
      },
    ),
  );
});

test('Oracle test', () => { // eslint-disable-line
  fc.assert(
    fc.property(
      fc.int32Array({
        max: 100,
        maxLength: 10,
        min: 0,
        minLength: 10,
      }), (generatedArray) => {
        // just it's bubble sorting
        const bubbleSorting = (inputArr) => {
          const len = inputArr.length;
          let swapped;
          do {
            swapped = false;
            for (let i = 0; i < len; i++) { // eslint-disable-line
              if (inputArr[i] > inputArr[i + 1]) {
                const tmp = inputArr[i];
                inputArr[i] = inputArr[i + 1]; // eslint-disable-line
                inputArr[i + 1] = tmp; // eslint-disable-line
                swapped = true;
              }
            }
          } while (swapped);
          return inputArr;
        };

        const sortedArr = sort(generatedArray);
        const sortedArr2 = bubbleSorting(generatedArray);
        expect(sortedArr).toEqual(sortedArr2); // eslint-disable-line
      },
    ),
  );
});

test('Error catching', () => { // eslint-disable-line
  fc.assert(
    fc.property(
      fc.string(), (generatedString) => {
        expect(() => sort(generatedString).toThrow(TypeError)) // eslint-disable-line
      },
    ),
  );
});

test('Jest sorting test', () => { // eslint-disable-line
  fc.assert(
    fc.property(
      fc.int32Array({
        max: 100,
        maxLength: 10,
        min: 0,
        minLength: 10,
      }), (generatedArray) => {
        expect(() => sort(generatedArray).toBeSorted()) // eslint-disable-line
      },
    ),
  );
});
