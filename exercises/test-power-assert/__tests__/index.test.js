const assert = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN
const arr = ['a', 'b', ['c', ['d', ['e', 'f']], 'g']];
const arr1 = ['a', 'b', 'c', ['d', ['e', 'f']], 'g'];
const arr2 = ['a', 'b', 'c', 'd', ['e', 'f'], 'g'];
const arr3 = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];

assert.deepEqual(flattenDepth(null), [], 'null should be array');
assert.deepEqual(flattenDepth({}), [], 'object should be array');
assert.deepEqual(flattenDepth([]), [], 'empty array should be empty array');
assert.deepEqual(flattenDepth(arr), arr1, 'function called without arguments returns flatten([], 1)');
assert.deepEqual(flattenDepth(arr, 1), arr1, 'function returns flatten([], 1) depth');
assert.deepEqual(flattenDepth(arr, 2), arr2, 'function returns flatten([], 2) depth');
assert.deepEqual(flattenDepth(arr, 3), arr3, 'function returns flatten([], 3) depth');

// END
