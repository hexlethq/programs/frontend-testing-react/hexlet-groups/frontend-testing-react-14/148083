test('Straight testing', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);
  expect(result).toEqual({ k: 'v', a: 'a', b: 'b' });
});

test('Object clone', () => {
  const src = { a: 1 };
  const result = Object.assign({}, src); // eslint-disable-line
  expect(result).toEqual(src);
});

test('Object assigning', () => {
  const src = { a: 1 };
  const target = { b: 2 };
  const result = Object.assign(src, target);
  expect(result).toEqual(src);
});

test('Copying symbolic key', () => {
  const src = { 'banana-lopala-bomba': 1 };
  const target = { b: 2 };
  const result = Object.assign(src, target);
  expect(result).toEqual({ 'banana-lopala-bomba': 1, b: 2 });
});

test('Ignore null', () => {
  const src = { a: 1 };
  const target = null;
  const result = Object.assign(src, target);
  expect(result).toEqual({ a: 1 });
});

test('Ignore undefined', () => {
  const src = { a: 1 };
  const target = null;
  const result = Object.assign(src, target);
  expect(result).toEqual({ a: 1 });
});

test('Ignore numbers', () => {
  const src = { a: 1 };
  const target = null;
  const result = Object.assign(src, target);
  expect(result).toEqual({ a: 1 });
});

test('Copying nested objects', () => {
  const src = { a: 1 };
  const target = { b: { c: 'd' } };
  const result = Object.assign(src, target);
  expect(result).toEqual({ a: 1, b: { c: 'd' } });
});

test('Copying self', () => {
  const src = { a: 1 };
  const result = Object.assign(src, src);
  expect(result).toEqual({ a: 1 });
});

test('Check for object link', () => {
  const src = { a: 1 };
  const target = { b: 2 };
  const result = Object.assign(src, target);
  expect(result).toBe(src);
});

test('Check TypeError without parameters', () => {
  expect(Object.assign).toThrow(TypeError);
});

test('Check TypeError with parameters', () => {
  expect(() => Object.assign(null, null)).toThrow(TypeError);
});
